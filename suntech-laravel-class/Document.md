* Kiểu Json (database: json)

{
	key1: value1,
	key2: value2,
	............
	............
	............
	keyN: valueN
}

System:
	NodeJs (Chat)
	Vue (Js) (Frontend)
	PHP (Backend)
	Java

	PHP vs JS  (Định dạng chung | Kiểu dữ liệu)
	PHP vs NodeJs  (Định dạng chung | Kiểu dữ liệu)
	Java vs PHP (Định dạng chung | Kiểu dữ liệu)

	Json ()

	Ajax (Json) PHP json_endcode() -> json_decode()
	Js:  Json object
	XML (trước) (Phức tạp)

* Session In Laravel
	// Khởi tạo (không cần)

	// Tạo mới session key
	$request->session()->put('session1', 'Nguyễn Văn A');
	$request->session()->put('session1', 'Nguyễn Văn C');

	$request->session()->push('session2', 'Nguyễn Văn B');
	$request->session()->push('session2', 'Nguyễn Văn D');

	// Lấy giá trị của 1 session key
	// $sessionOne = $request->session()->get('session1');
	// dd($sessionOne);

	// Kiểm tra sự tồn tại của Session
	dump($request->session()->has('session3'));

	dump(session()->exists('session4'));

	session(['iphone' => 12]);

	// Xóa một và nhiều session
	// session()->pull('iphone', 'session2');
	// session()->forget('session2');
	// session()->forget(['session1', 'session2']);

	// session()->flush();

	// Lấy ra tất session đang có
	// $all = $request->session()->all();
	$all = Session::all();
	dump($all);
	
* Collection In Laravel
	ORM: Result collection

	Tạo collection trong Laravel
	