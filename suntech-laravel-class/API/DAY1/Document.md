API:

1. Authentication (Login & Logout)
   JWT (Json Web Token)

2. Category (Auth Require)
    - List
    - Create
    - Update
    - Delete

3. Product (Auth Require)
    - List
    - Create
    - Update
    - Delete
    - Upload Image

4. Order (Auth Require)
    - Read
    - Update

5. Slider (Auth Require)
    - List
    - Create
    - Update
    - Delete

6. User (Auth Require)
    - List
    - Create
    - Update
    - Delete

7. Setting
    - Read
    - Create/Update
