<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Frontend')->group(function() {
    Route::get('/', 'HomeController@index')
        ->name('frontend.home');
    
    Route::get('gio-hang', 'CartController@index')
        ->name('cart.index');

    Route::get('cart/{productId}', 'CartController@store')
        ->name('cart.store');

    Route::post('cart/update', 'CartController@update')
        ->name('cart.update');

    Route::get('cart/remove-product/{productId}', 'CartController@destroy')
        ->name('cart.destroy');

    Route::post('cart/checkout', 'CartController@checkout')
        ->name('cart.checkout');

    Route::get('product/show/{id}', 'ProductController@show')
        ->name('product.show');    

    Route::get('{slug}', 'CategoryController@show')
        ->name('frontend.category.show');
});
