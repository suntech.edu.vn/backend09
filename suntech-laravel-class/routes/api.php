<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware(['api'])->prefix('v1')->group(function ($router) {
    
    Route::post('login', 'AuthController@login');
    Route::delete('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('upload', 'UploadController@upload')->name('upload');
    
    Route::middleware(['token.valid'])->group(function() {
        Route::resource('category', 'CategoryController')
            ->except(['show', 'edit', 'create']);

        Route::resource('product', 'ProductController')
            ->except(['show', 'edit', 'create']);

        Route::resource('slider', 'SliderController')
            ->except(['show', 'edit', 'create']);

        Route::resource('order', 'OrderController')
            ->except(['edit', 'create']);
    });
});